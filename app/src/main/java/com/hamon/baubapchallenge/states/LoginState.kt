package com.hamon.baubapchallenge.states

sealed class LoginState {
    object Init : LoginState()
    object UserFieldEmptyError : LoginState()
    object PassFieldEmptyError : LoginState()
    object UserAndPassFieldsEmptyError : LoginState()
    object UserAndPassError : LoginState()
    object UserLogged : LoginState()
}