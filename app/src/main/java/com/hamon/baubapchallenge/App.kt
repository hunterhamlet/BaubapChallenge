package com.hamon.baubapchallenge

import androidx.multidex.MultiDexApplication
import com.hamon.baubapchallenge.di.viewModels
import com.hamon.provider.di.repositories
import com.hamon.provider.di.useCases
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@App)
            androidFileProperties()
            koin.apply {
                loadModules(
                    arrayListOf(
                        repositories,
                        useCases,
                        viewModels
                    )
                )
                createRootScope()
            }
        }
    }

}