package com.hamon.baubapchallenge.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.hamon.baubapchallenge.R
import com.hamon.baubapchallenge.databinding.FragmentLoginBinding
import com.hamon.baubapchallenge.extensions.cleanError
import com.hamon.baubapchallenge.extensions.hideKeyboard
import com.hamon.baubapchallenge.extensions.showError
import com.hamon.baubapchallenge.states.LoginState
import com.hamon.baubapchallenge.viewModels.LoginViewModel
import com.hamon.provider.models.User
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : Fragment() {

    private val binding: FragmentLoginBinding by lazy {
        FragmentLoginBinding.inflate(layoutInflater)
    }
    private val viewModel by viewModel<LoginViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupOnCLickListeners()
        setupObservables()
        setupTextChangeListener()
    }

    private fun setupOnCLickListeners() {
        binding.apply {
            buttonLogin.setOnClickListener {
                viewModel.checkUser(
                    User(
                        user = textInputUser.text.toString(),
                        password = textInputPassword.text.toString()
                    )
                )
            }
            textInputPassword.setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE -> {
                        hideKeyboard()
                        viewModel.checkUser(
                            User(
                                user = textInputUser.text.toString(),
                                password = textInputPassword.text.toString()
                            )
                        )
                        true
                    }
                    else -> false
                }
            }
        }
    }

    private fun setupObservables() {
        viewModel.loginState.observe(viewLifecycleOwner) { handleLoginState(it) }
    }

    private fun setupTextChangeListener() {
        binding.apply {
            textInputUser.doOnTextChanged { text, _, _, _ ->
                if (text.isNullOrEmpty()) {
                    handleUserEmptyError()
                } else {
                    viewModel.cleanState()
                }
            }
            textInputPassword.doOnTextChanged { text, _, _, _ ->
                if (text.isNullOrEmpty()) {
                    handlePassEmptyError()
                } else {
                    viewModel.cleanState()
                }
            }
        }
    }

    private fun handleLoginState(state: LoginState) {
        when (state) {
            is LoginState.Init -> handleInit()
            is LoginState.UserFieldEmptyError -> handleUserEmptyError()
            is LoginState.UserAndPassError -> handleUserAndPassError()
            is LoginState.PassFieldEmptyError -> handlePassEmptyError()
            is LoginState.UserAndPassFieldsEmptyError -> handleUserAndPassEmptyError()
            is LoginState.UserLogged -> handleUserLogged()
        }
    }

    private fun handleUserEmptyError() {
        binding.textInputUserLayout.showError(getString(R.string.login_user_empty_error))
    }

    private fun handlePassEmptyError() {
        binding.textInputPasswordLayout.showError(getString(R.string.login_pass_empty_error))
    }

    private fun handleUserAndPassEmptyError() {
        handleUserEmptyError()
        handlePassEmptyError()
    }

    private fun handleUserAndPassError() {
        showNeutralDialog(
            title = getString(R.string.login_user_pass_error),
            message = getString(R.string.login_message_error),
            titleNeutralButton = getString(R.string.login_close_dialog)
        )
    }

    private fun handleUserLogged() {
        showNeutralDialog(
            title = getString(R.string.login_success),
            message = getString(R.string.login_message),
            titleNeutralButton = getString(R.string.login_close_dialog)
        )
    }

    private fun handleInit() {
        binding.apply {
            textInputUserLayout.cleanError()
            textInputPasswordLayout.cleanError()
        }
    }

    private fun showNeutralDialog(title: String, message: String, titleNeutralButton: String) {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(title)
            .setMessage(message)
            .setNeutralButton(titleNeutralButton) { dialog, _ ->
                dialog.dismiss()
            }.show()
    }

}