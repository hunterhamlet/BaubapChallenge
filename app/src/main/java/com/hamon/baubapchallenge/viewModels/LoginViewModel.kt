package com.hamon.baubapchallenge.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hamon.baubapchallenge.states.LoginState
import com.hamon.provider.models.User
import com.hamon.provider.useCases.LoginUseCase

class LoginViewModel(private val loginUseCase: LoginUseCase) : ViewModel() {

    private val _loginState: MutableLiveData<LoginState> = MutableLiveData()
    val loginState: LiveData<LoginState> get() = _loginState

    fun checkUser(user: User) {
        _loginState.value = handleUserState(user)
    }

    private fun handleUserState(user: User): LoginState {
        return when {
            user.password.isEmpty().not() && user.user.isEmpty().not() -> checkUserCredentials(user)
            user.user.isEmpty() && user.password.isEmpty().not() -> LoginState.UserFieldEmptyError
            user.password.isEmpty() && user.user.isEmpty().not() -> LoginState.PassFieldEmptyError
            else -> LoginState.UserAndPassFieldsEmptyError
        }
    }

    private fun checkUserCredentials(user: User): LoginState {
        return if (loginUseCase.invoke(user)) {
            LoginState.UserLogged
        } else {
            LoginState.UserAndPassError
        }
    }

    fun cleanState() {
        _loginState.value = LoginState.Init
    }
}