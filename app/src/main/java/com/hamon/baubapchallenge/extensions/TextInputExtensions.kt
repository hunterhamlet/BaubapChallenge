package com.hamon.baubapchallenge.extensions

import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

fun TextInputEditText.checkIsNotEmptyOrNull(): Boolean = this.text.isNullOrEmpty().not()

fun TextInputLayout.showError(error: String) {
    this.error = error
}

fun TextInputLayout.cleanError() {
    this.error = null
}