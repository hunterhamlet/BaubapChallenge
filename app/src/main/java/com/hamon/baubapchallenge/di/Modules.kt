package com.hamon.baubapchallenge.di

import com.hamon.baubapchallenge.viewModels.LoginViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModels = module {
    viewModel { LoginViewModel(get()) }
}