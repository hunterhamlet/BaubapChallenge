package com.hamon.baubapchallenge

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.hamon.baubapchallenge.states.LoginState
import com.hamon.baubapchallenge.viewModels.LoginViewModel
import com.hamon.provider.models.User
import com.hamon.provider.useCases.LoginUseCase
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LoginViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var loginUseCase: LoginUseCase

    private lateinit var loginViewModel: LoginViewModel

    private val userMockError = User(user = "pakita.kabeza", password = "laloLanda")
    private val userAndPassEmptyMockError = User(user = "", password = "")
    private val passEmptyMockError = User(user = "pakita.kabeza", password = "")
    private val userEmptyMockError = User(user = "", password = "laloLanda")
    private val userMockSuccess = User(user = "hunter.hamon", password = "laPataSalta")

    @Before
    fun setup() {
        loginViewModel = LoginViewModel(loginUseCase)
    }

    @Test
    fun shouldUserEmptyError() {
        loginViewModel.checkUser(userEmptyMockError)
        assertEquals(loginViewModel.loginState.value, LoginState.UserFieldEmptyError)
    }

    @Test
    fun shouldPassEmptyError() {
        loginViewModel.checkUser(passEmptyMockError)
        assertEquals(loginViewModel.loginState.value, LoginState.PassFieldEmptyError)
    }

    @Test
    fun shouldUserAndPassEmptyError() {
        loginViewModel.checkUser(userAndPassEmptyMockError)
        assertEquals(loginViewModel.loginState.value, LoginState.UserAndPassFieldsEmptyError)
    }

    @Test
    fun shouldUserOrPassError() {
        loginViewModel.checkUser(userMockError)
        assertEquals(loginViewModel.loginState.value, LoginState.UserAndPassError)
    }

    @Test
    fun shouldSuccess() {
        given(loginUseCase.invoke(userMockSuccess)).willReturn(true)
        loginViewModel.checkUser(userMockSuccess)
        assertEquals(loginViewModel.loginState.value, LoginState.UserLogged)
    }

}