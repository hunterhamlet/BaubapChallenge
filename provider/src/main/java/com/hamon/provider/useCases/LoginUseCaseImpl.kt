package com.hamon.provider.useCases

import com.hamon.provider.models.User

internal class LoginUseCasesImpl(private val loginRepository: LoginRepository) : LoginUseCase {
    override fun invoke(user: User): Boolean = loginRepository.userLogin(user)
}

interface LoginUseCase {
    operator fun invoke(user: User): Boolean
}

interface LoginRepository {
    fun userLogin(user: User): Boolean
}