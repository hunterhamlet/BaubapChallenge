package com.hamon.provider.repositories

import com.hamon.provider.BuildConfig
import com.hamon.provider.models.User
import com.hamon.provider.useCases.LoginRepository

internal class LoginRepositoryImpl : LoginRepository {

    override fun userLogin(user: User): Boolean {
        return user.user == BuildConfig.USER && user.password == BuildConfig.PASSWORD
    }
}