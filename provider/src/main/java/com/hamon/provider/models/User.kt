package com.hamon.provider.models

data class User(
    val user: String,
    val password: String
)