package com.hamon.provider.di

import com.hamon.provider.repositories.LoginRepositoryImpl
import com.hamon.provider.useCases.LoginRepository
import com.hamon.provider.useCases.LoginUseCase
import com.hamon.provider.useCases.LoginUseCasesImpl
import org.koin.dsl.module

val repositories = module {
    single<LoginRepository> { LoginRepositoryImpl() }
}

val useCases = module {
    single<LoginUseCase> { LoginUseCasesImpl(get()) }
}