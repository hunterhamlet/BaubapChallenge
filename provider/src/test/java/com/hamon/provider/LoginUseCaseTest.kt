package com.hamon.provider

import com.hamon.provider.models.User
import com.hamon.provider.useCases.LoginRepository
import com.hamon.provider.useCases.LoginUseCase
import com.hamon.provider.useCases.LoginUseCasesImpl
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LoginUseCaseTest {

    @Mock
    lateinit var loginRepository: LoginRepository

    private lateinit var loginUseCase: LoginUseCase

    private val userMockError = User(user = "pakita.kabeza", password = "laloLanda")
    private val userMockSuccess = User(user = "hunter.hamon", password = "laPataSalta")

    @Before
    fun setup() {
        loginUseCase = LoginUseCasesImpl(loginRepository)
    }

    @Test
    fun shouldErrorUserOrPass() {
        given(loginRepository.userLogin(userMockError)).willReturn(false)
        val result = loginUseCase.invoke(userMockError)
        assertFalse(result)
    }

    @Test
    fun shouldSuccess() {
        given(loginRepository.userLogin(userMockSuccess)).willReturn(true)
        val result = loginUseCase.invoke(userMockSuccess)
        assertTrue(result)
    }

}